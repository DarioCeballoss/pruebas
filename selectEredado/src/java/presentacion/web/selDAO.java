package presentacion.web;

import java.sql.*;
import java.util.*;

public class selDAO {
//    static Object consultar() {
    static ArrayList consultar() throws Exception {
            ArrayList listado = new ArrayList();
            Connection conectar = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conectar = DriverManager.getConnection(
               "jdbc:mysql://localhost/select_db",
               "educacion","educacion");
            
            PreparedStatement sentencia = 
                    conectar.prepareStatement(
                    " SELECT id_estado, estado FROM estado ORDER BY estado ASC ");
            
            ResultSet resulSQL = sentencia.executeQuery();
            
            //int i = 30;
            //while( 0 < i--){
            while( resulSQL.next() ){
              TreeMap elEstado = new TreeMap();
              elEstado.put("estado", resulSQL.getString("estado"));
              elEstado.put("id", resulSQL.getString("id_estado"));
            
              listado.add(elEstado);
            }
            
            return listado;
    }
    
}
